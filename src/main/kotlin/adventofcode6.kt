import java.net.URL
import kotlin.math.floor
import kotlin.math.pow

fun main(args: Array<String>) {
    val inputFile = object {}.javaClass.getResource("day12.txt")
    day12(inputFile)

//    Day07.part1(inputFile.readText().lines())
//    Day07.part2(inputFile.readText().lines())
}

object Day07 {
    private fun getBidsMultiplication(
        inputs: List<String>,
        labelValues: Map<Char, Char>,
        getTypeScore: (String) -> Int)
    {
        println(
            inputs
                .map { line ->
                    val (hand, bid) = line.split(" ")
                    Triple(
                        getTypeScore(hand),
                        labelValues.entries.fold(hand) { acc, (key, value) -> acc.replace(key, value) },
                        bid.toInt()
                    )
                }
                .sortedWith(compareBy({ it.first }, { it.second }))
                .map { it.third }
                .reduceIndexed { index, acc, bid -> acc + bid * (index + 1) }
        )
    }

    fun part1(inputs: List<String>) {
        val labelValues = mapOf(
            'A' to 'E',
            'K' to 'D',
            'Q' to 'C',
            'J' to 'B',
            'T' to 'A'
        )

        fun getTypeScore(hand: String): Int {
            val labelCounts = hand.toSet().map { char -> char to hand.count { it == char } }
            val maxCount = labelCounts.maxOf { it.second }

            if (labelCounts.map { it.second }.reduce { acc, x -> acc * x } == 6) {
                return 5
            } else if (labelCounts.count {it.second == 2} == 2) {
                return 3
            }

            return when (maxCount) {
                1, 2 -> maxCount
                3 -> maxCount + 1
                4, 5 -> maxCount + 2
                else -> throw Exception()
            }
        }

        getBidsMultiplication(inputs, labelValues) { getTypeScore(it) }
    }

    fun part2(inputs: List<String>) {
        val labelValues = mapOf(
            'A' to 'D',
            'K' to 'C',
            'Q' to 'B',
            'T' to 'A',
            'J' to '1'
        )

        fun isFull(labelCounts: List<Pair<Char, Int>>, jokerCount: Int): Boolean =
            labelCounts.map { it.second }.reduce { acc, x -> acc * x } == 6 ||
                    labelCounts.size == 2 &&
                    ((labelCounts[0].second + jokerCount) * labelCounts[1].second == 6 ||
                            (labelCounts[1].second + jokerCount) * labelCounts[0].second == 6)

        fun isTwoPair(labelCounts: List<Pair<Char, Int>>, jokerCount: Int): Boolean =
            labelCounts.count {it.second == 2} == 2 ||
                    labelCounts.count {it.second == 2} == 1 && jokerCount > 0


        fun getTypeScore(hand: String): Int {
            val labelCounts = hand.toSet().map { char -> char to hand.count { it == char } }.filter { it.first != 'J' }
            val jokerCount = hand.count { it == 'J' }
            val maxCount = if (jokerCount == 5) 5 else labelCounts.maxOf { it.second + jokerCount }

            return if (maxCount == 5 || maxCount == 4) {
                maxCount + 2
            } else if (isFull(labelCounts, jokerCount)) {
                5
            } else if (maxCount == 3) {
                maxCount + 1
            } else if (isTwoPair(labelCounts, jokerCount)) {
                3
            } else {
                maxCount
            }
        }

        getBidsMultiplication(inputs, labelValues) { getTypeScore(it) }
    }
}


fun day12(inputFile: URL) {
    val lines = inputFile.readText().lines()
    lines.forEach { line ->
        val (left, right) = line.split(" ")
        val rightList = right.split(",")
        left.withIndex().toList().forEach { iv ->
            println(iv)
            if(iv.value == '?') {

            }
        }

        println(rightList)
        return
    }
}

fun day7(inputFile: URL) {
    var sum = 0

    val valuedMap = HashMap<Int, ArrayList<Pair<String, Int>>>()
    val resultList = ArrayList<Pair<String, Int>>()
    valuedMap[1] = arrayListOf()
    valuedMap[2] = arrayListOf()
    valuedMap[3] = arrayListOf()
    valuedMap[4] = arrayListOf()
    valuedMap[5] = arrayListOf()
    valuedMap[6] = arrayListOf()
    valuedMap[7] = arrayListOf()
    val lines = inputFile.readText().lines()
    lines.forEach { line->
        val (hand, bid) = line.split(" ")
        val charMap = HashMap<Char, Int>()
        hand.forEach {
            if(charMap.containsKey(it)) {
                charMap[it] = charMap[it]!!+1
            } else {
                charMap[it] = 1
            }
        }
        val resultMap = charMap.toList().sortedBy { (key,value) -> value }.reversed().toMap()
        if(resultMap.size == 5) { //High card
            valuedMap[1]?.add(Pair(hand,bid.toInt()))
        } else if(resultMap.containsValue(5)) { //Five of a kind
            valuedMap[7]?.add(Pair(hand,bid.toInt()))
        } else if(resultMap.containsValue(4)) { // Four of a kind
            valuedMap[6]?.add(Pair(hand,bid.toInt()))
        } else if(resultMap.size == 2 && resultMap.containsValue(3)) { //Full house
            valuedMap[5]?.add(Pair(hand,bid.toInt()))
        } else if(resultMap.size == 3 && resultMap.containsValue(3)) { // Three of a kind
            valuedMap[4]?.add(Pair(hand,bid.toInt()))
        } else if(resultMap.size == 4 && resultMap.containsValue(2)) { // one pair
            valuedMap[2]?.add(Pair(hand,bid.toInt()))
        } else if(resultMap.size == 3 && resultMap.containsValue(2)) { // two pair
            valuedMap[3]?.add(Pair(hand,bid.toInt()))
        }
        valuedMap.map { it.value.sortedWith(myCustomComparator) }
//        valuedMap.forEach { (s, pairs) ->
//            valuedMap[s]?.sortedWith(myCustomComparator)
//            //if(s==6) println(valuedMap[s]?.sortedWith(myCustomComparator))
//        }
    }

    valuedMap.forEach { (s, pairs) ->
        resultList.addAll(pairs)
    }
    //println(resultList)
    resultList.forEachIndexed { index, pair ->
        sum += (index.plus(1)).times(pair.second)
    }
        println(sum)
}

private val myCustomComparator = Comparator<Pair<String, Int>> { a, b ->
    val sortOrder = "AKQJT98765432"
    val s1 = a.first
    val s2 = b.first
    for(i in 0..4) {
        when {
            (sortOrder.indexOf(s1[i]) < sortOrder.indexOf(s2[i])) -> return@Comparator 1
            (sortOrder.indexOf(s1[i]) > sortOrder.indexOf(s2[i])) -> return@Comparator -1
        }
    }
    return@Comparator 0
}



fun day4() {
    var sum = 0
    val inputFile = object {}.javaClass.getResource("day4.txt")
    val lines = inputFile.readText().lines()
    lines.forEach { line ->
        val winList = ArrayList<Int>()
        val myList = ArrayList<Int>()
        val cardString = line.substringAfter(": ")
        val (winningNumbers, myNumbers) = cardString.split(" | ")
        winningNumbers.split(" ").forEach {
            if(it.isNotEmpty()) {
                winList.add(it.toInt())
            }
        }
        myNumbers.split(" ").forEach {
            if(it.isNotEmpty()) {
                myList.add(it.toInt())
            }
        }
        val punkte = floor(2.0.pow(winList.intersect(myList).size-1))
        sum +=punkte.toInt()
    }
    println("Ergebnis1: $sum")
}

fun day3() {
    var lastLine = ""
    val inputFile = object {}.javaClass.getResource("day3.txt")
    val availableSymbols = arrayListOf('*', '$', '&', '=', '#', '@', '/', '+', '-', '%')
    val lines = inputFile.readText().lines()
    lines.forEachIndexed { linesIndex, line ->
        println(line)
        line.forEachIndexed { index, char ->
            var currentNumber = 0
            if(char.isDigit()) {
                val charBefore = if(index == 0) 'X' else line[index-1]
                val charAfter = if((index+1)>=line.length) 'X' else line[index+1]
                if(availableSymbols.contains(charBefore)
                    || availableSymbols.contains(charAfter)
//                    || lines[linesIndex-1]
                    //|| charAfter.isDigit()
                    ) {
                    println("${char.digitToInt()} hat kontakt")
                }
                currentNumber = char.digitToInt()

            }
        }
        val symbols = line.filter { !it.isDigit() && it != '.' }
        symbols.forEach { symbol ->
            val index = line.indexOf(symbol)
        }

        lastLine = line
    }
}

fun day2() {
    var sum = 0
    var power = 0
    val maxRed = 12
    val maxGreen = 13
    val maxBlue = 14
    val inputFile = object {}.javaClass.getResource("day2.txt")
    inputFile.readText().lines().forEach { line ->
        var currentMaxGreen = 0
        var currentMaxRed = 0
        var currentMaxBlue = 0
        val gameId = line.substringBefore(": ").filter { it.isDigit() }.toInt()
        val fullGameContent = line.substringAfter(": ")
        val sets = fullGameContent.split(';')
        sets.forEach {
            val einWuerfel = it.split(',')
            einWuerfel.forEach { wuerfel ->
                val newValue = wuerfel.filter { it.isDigit() }.toInt()
                if(wuerfel.contains("green")) {
                    if(newValue > currentMaxGreen) {
                        currentMaxGreen = newValue
                    }
                }
                if(wuerfel.contains("red")) {
                    if(newValue > currentMaxRed) {
                        currentMaxRed = newValue
                    }
                }
                if(wuerfel.contains("blue")) {
                    if(newValue > currentMaxBlue) {
                        currentMaxBlue = newValue
                    }
                }
            }
        }
         power += currentMaxBlue.times(currentMaxGreen).times(currentMaxRed)

        if(currentMaxBlue <= maxBlue
            && currentMaxGreen <= maxGreen
            && currentMaxRed <= maxRed) {
            println(gameId)
            sum += gameId
        }
    }
    println("Ergebnis1: $sum")
    println("Ergebnis2: $power")
}

fun day1() {
    var sum = 0
    val inputFile = object {}.javaClass.getResource("day1.txt")
    val digits = arrayListOf("zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine")
    inputFile.readText().lines().forEach {line ->
        val digitsMap = HashMap<Int, Int>()
        digits.forEachIndexed { index, value ->
            val indexOfWord = line.indexOf(value)
            if(indexOfWord != -1) {
                digitsMap[indexOfWord] = index
            }
            val indexOfWord2 = line.lastIndexOf(value)
            if(indexOfWord2 != -1) {
                digitsMap[indexOfWord2] = index
            }
        }
        line.forEachIndexed { index, value ->
            if(value.isDigit()) {
                digitsMap[index] = value.digitToInt()
            }
        }

        val entries = digitsMap.toSortedMap().entries
        println(line)
        println(entries)
        val first = entries.first().value
        val last = entries.last().value
        val resultNumber = "$first$last".toInt()
        sum += resultNumber
    }
    println(sum)

}

fun day6() {
    val inputFile = object {}.javaClass.getResource("day6.txt")
    val x1 = calculateRecordCount(42, 308)
    val x2 = calculateRecordCount(89, 1170)
    val x3 = calculateRecordCount(91, 1291)
    val x4 = calculateRecordCount(89, 1467)
    println("Ergebnis 1: ${x1.times(x2).times(x3).times(x4)}")
    val x5 = calculateRecordCount(42899189, 308117012911467)
    println("Ergebnis 2: $x5")
}
fun calculateRecordCount(raceTimeMs:Long, recordDistance:Long): Int {
    var recordCount = 0
    var pressButtonMs:Int = 0
    var speed:Int = 0
    var distance:Long = 0
    while(pressButtonMs<=raceTimeMs) {
        val moveTime = raceTimeMs-pressButtonMs
        speed = pressButtonMs // (mm/ms)
        distance = moveTime.times(speed)
        if(distance > recordDistance) {
            recordCount++
        }
        pressButtonMs++
    }
    return recordCount
}